---
title: "Informations concernant la procédure de nomination d'un⋅e nouveau⋅elle directeur⋅rice"
layout: post
date: 2020-10-02 12:00                           # Laissez 2099 si c'est un brouillon. Pour l'heure,
                                                 # mettez au moins 2h avant l'heure actuelle (bug GitLab)
image: /assets/images/profile_pictures/CA.png   # Apparaît lorsque vous partagez le lien
headerImage: false                               # Mettez 'true' pour mettre l'image précédente en entête du post
tag:
  - CA
category: blog                                   # Mettez 'projects' pour mettre le post dans Projets
author: elusca
---

# Démission de Philippe Courtier
Durant le CA de juin, Philippe Courtier (directeur de l'UTC) a annoncé sa démission prochaine aux membres du conseil. Celle-ci a été officialisée lors du CA exceptionnel de juillet avec l'annonce de son départ à compter du 1er septembre pour un poste au sein du Ministère de la Transition Ecologique. Vous n'avez pas reçu de mail de la part de Philippe Courtier lors de son départ ni pour vous informer de celui-ci ni pour en expliquer les raisons donc nous nous permettons d'éclaircir la situation dans ce post.

Le CA exceptionnel de juillet avait trois buts:
- Sélectionner un cabinet de recrutement pour apporter à l'UTC les meilleurs candidat⋅e⋅s pour reprendre la direction de l'UTC
- Nommer un⋅e administrateur⋅trice provisoire pour occuper l'intérim d'ici la nomination
- Définir le calendrier de la procédure de nomination


# Sélection du cabinet
L'UTC a lancé un appel d'offres en prenant en compte: la réputation du cabinet, les services proposés et le prix. L'UTC a retenu le cabinet le plus réputé qui présentait un coût proche des autres. Ce cabinet est reconnu pour avoir recruté des directeurs pour les plus grandes entreprises et les plus grandes écoles. 


# Nomination de l'administratrice provisoire
A la fin de l'été, vous (mis à part les nouveaux étudiants) avez reçu un mail vous annonçant que Claire Rossi était nommée administratrice provisoire. Le choix de Claire Rossi avait été proposé par le CA puis envoyé au Ministère de l'Enseignement Supérieur et au rectorat d'Amiens. Sachez que sans proposition du CA, le Ministère trouve le plus souvent une personne extérieure à l'établissement pour assurer l'intérim. La CA tenait à ce qu'une personne interne à l'UTC assure cette intérim particulièrement au coeur d'une crise COVID qui demande une proximité et une compréhension de l'UTC.


# Calendrier de la procédure de nomination
Au sein du CA, une commission *ad hoc* a été constituée afin d'accompagner cette procédure et notamment pour étudier les candidatures et en restituer la substance au reste du CA. Le représentant étudiant au sein de la commission est Marc DAMIE. Au sein de cette commission, il s'efforcera de représenter au mieux les étudiant⋅es et nottamment de s'assurer que les idées portées par les listes ayant remporté des sièges aux dernières élections universitaires ne soient pas négligées. Actuellement, la fiche de poste du directeur a été publiée et les candidatures sont ouvertes. Voici les étapes à venir:

- **9 octobre**: Date limite de dépot des candidatures
- **du 12 au 23 octobre**: Le cabinet de recrutement étudie les candidatures
- **26 octobre**: remise de l'étude des candidatures par le cabinet
- **2 novembre**: réunion de la commission *ad hoc* pour étudier les candidats et élaborer une "short list" de candidat⋅e⋅s
- **6 novembre**: CA exceptionnel durant lequel la liste complète des candidats sera présentée et la "short list" devra être validée par le CA
- **du 9 novembre au 13 décembre**: la commission *ad hoc* rencontrera chacun⋅e des candidat⋅e⋅s de la "short list" et les candidat⋅e⋅s seront libres de se rendre à l'UTC pour rencontrer directeurs fonctionnels, étudiants, enseignants, etc.
- **18 décembre**: CA exceptionnel durant lequel les candidat⋅e⋅s de la "short list" seront auditionné⋅e⋅s. Le CA proposera une liste ordonnée (par ordre de préférence) avec, au maximum, trois noms. Cette liste sera envoyée au Ministère de l'Enseignement Supérieur.
- **Janvier 2021**: normalement, le Ministère nommera le premier nom de la liste comme directeur.rice de l'UTC.

A l'issue de ce calendrier, le⋅la prochain⋅e directeur⋅rice de l'UTC pourrait arriver dès la rentrée de février mais cela dépendra fortement de la disponibilité de le⋅la candidat⋅e qui pourrait avoir une période de pré-avis ne lui permettant pas d'arriver si tôt.

Nous écrirons un nouveau post une fois la "short list" validée afin que vous ayez connaissance des candidat⋅e⋅s qui la composent et de leurs CV respectifs.

Afin d'échanger entre étudiant⋅es à sur ce sujet, nous vous invitons à rejoindre le canal dédiée sur l'équipe Mattermost des élu⋅es étudiant⋅es : [https://team.picasoft.net/elues-etu-utc/channels/1---ca](https://team.picasoft.net/elues-etu-utc/channels/1---ca) . Vous pouvez également nous conctacter par mail: elusca@utc.fr
