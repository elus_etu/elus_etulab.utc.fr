---
title: "Le CÉVU"
layout: post
date: 2021-03-25 12:00                           # Laissez 2099 si c'est un brouillon. Pour l'heure,
                                                 # mettez au moins 2h avant l'heure actuelle (bug GitLab)
image: /assets/images/profile_pictures/all.png   # Apparaît lorsque vous partagez le lien
headerImage: false                               # Mettez 'true' pour mettre l'image précédente en entête du post
category: about                                  # Mettez 'projects' pour mettre le post dans Projets
author: eluscevu
---

<p style="text-align: center;">
  <code style="color:#666;background:transparent;font-size:large">
    · Qu'est-ce que le CÉVU ? ·
  </code>
</p>

👨‍🏫Le CÉVU (Conseil des Études et de la Vie Universitaire) est concerté sur des questions concernant les enseignements, leur organisation, leurs règles d'évaluation, leurs modifications. Il prépare et donne son avis sur les projets de nouveaux enseignements/filières. Il s'occupe également des questions en rapport avec la vie étudiante, la bibliothèque, les oeuvres universitaires et scolaires. Par exemple, le CÉVU s'occupe de proposer une utilisation de la CVEC (Contribution de vie étudiante et de campus). 📚

👥Le CÉVU a aussi pour but d'accompagner les étudiants dans leur entrée dans la vie étudiante, mais également sur toute la durée de leur scolarité à l'UTC. 

📌Le CÉVU se réunit environ une fois par mois, le jeudi après-midi.

💶Le CÉVU donne son avis sur la répartition de l'enveloppe des moyens destinée à la formation.
Il est en charge du FSDIE (Le Fond de Solidarité et de Développement des Initiatives Etudiantes).

![]({{site.url}}/assets/images/posts/fonctions-cevu.png)

Le fond du FSDIE est financé par une partie des frais de scolarité des étudiants, que la commission CVEC a fléché pour le FSDIE (16€ par étudiant ). Il est divisé en deux sous commissions qui vont voter la répartition du budget ⚖️:

✔️*La commission projets étudiants* : les associations de l'UTC demandent des fonds pour des projets (chaque projet doit être affilié à un pôle de la fédération du BDE-UTC). Les étudiants en charge du budget du projet déposent une demande de subvention sur un site dédié sur l'ENT. Y siègent des élus étudiants du CÉVU, un membre du BDE, des personnels de l'UTC impliqués dans la vie associative, le VP CÉVU qui préside la commission ainsi que des personnalités extérieures.

✔️*La commission sociale* vient en aide aux étudiants en difficulté financière ou traversant une période difficile. 🤝 Elle se réunit en cas de demande d'aide d'étudiants, et les membres de la commission attribuent le fond social du FSDIE (minimum 10% du fond total) si celle-ci est légitime. Cette commission est composée de membres de l'administration, de représentants des étudiants (élus CÉVU et éventuellement CA), des personnalités extérieures (le CROUS). Elle peut très bien ne pas se réunir au cours d’une année, alors cette somme est utilisé dans le cadre du FSDIE "projet étudiants". D'ailleurs, elle n'est que très rarement appelée (avis aux étudiants en difficulté, les élus peuvent vous aider !).

![]({{site.url}}/assets/images/posts/fsdie.png)

👩‍💻 Le CÉVU est composé de 29 membres donc 11 étudiants titulaires (on rajoute donc à ce chiffre 11 étudiants suppléants). En plus des étudiants, il est composé d'élus enseignants et personnels. 👩‍🏫👨‍💼 Parmis tous les élus, on trouve le VP (Vice Président) et le VPE (Vice Président Etudiant) du CÉVU, qui sont élus par les 29 membres du CÉVU tous les 2 ans. Ils seront élus le 28 novembres pour le nouveau CÉVU . Vous retrouverez la composition des élus du CÉVU en photo ci-dessous. Ce sont les élus qui votent les décisions. Les suppléants sont invités aux séances du CÉVU. 👩‍⚖️

💬 On y retrouve également au CÉVU des invités permanents, qui donnent leur avis sur les décisions prises :
- Christophe GUY en tant que directeur de l’UTC et président du CÉVU
- Claire ROSSI, directeur adjoint de l’UTC
- Nathalie Van SCHOOR, directrice générale des services (DGS)
- Véronique HEDOU, responsable vie associative, enseignante-chercheuse
- Etienne ARNOULT et Antoine JOUGLET, directeurs de la formation et de la pédagogie (DFP)

![]({{site.url}}/assets/images/posts/repartition-cevu.png)
