---
title: "Les instances de l'UTC"
layout: post
date: 2021-03-24 12:00                           # Laissez 2099 si c'est un brouillon. Pour l'heure,
                                                 # mettez au moins 2h avant l'heure actuelle (bug GitLab)
image: /assets/images/profile_pictures/all.png   # Apparaît lorsque vous partagez le lien
headerImage: false                               # Mettez 'true' pour mettre l'image précédente en entête du post
category: about                                  # Mettez 'projects' pour mettre le post dans Projets
author: eluscevu
---

<p style="text-align: center;">
  <code style="color:#666;background:transparent;font-size:large">
    · Quelles sont les instances de l'UTC ? ·
  </code>
</p>

Aujourd'hui nous avons choisi de vous expliquer en globalité le rôle des différentes instances décisionnelles de l'UTC. A la fin de ce post, vous saurez donc où sont prises les décisions à l'UTC.

## Les instances clés de l'UTC sont donc les suivantes :

🔹Le CA (Conseil d'Administration), son champ d'action se concentre autour de la politique générale de l'établissement. Le CÉVU, le CS et le Comité de Direction suivent les axes définis par le CA. Il vote par exemple la façon dont est utilisé le budget.

🔹Le rôle du CÉVU (Conseil des Études et de la Vie Universitaire) tourne autour de la vie universitaire et des enseignements. Il approuve par exemple la création ou la suppression de filière.

🔹Le CS (Conseil Scientifique) concerne plutôt la recherche, et fait le lien entre enseignement et recherche.

🔹Le Comité de Direction assiste le directeur dans la mise en oeuvre de la politique définie par le CA.

🔹Quant aux BDD (Bureaux de Départements), il y en a un pour chaque branche et un pour le département TSH, et ils gèrent les aspects de la formation liés plus spécifiquement à ces branches.

🔹Le Conseil de perfectionnement du master définit les grandes orientations pédagogiques du master.

🔹Le CED (Conseil de l'Ecole Doctorale) gère le fonctionnement de l'école doctorale.

🔹Le Directoire coordonne les gros projets de l'UTC.

![]({{site.url}}/assets/images/posts/fonctionnement-instances.png)
