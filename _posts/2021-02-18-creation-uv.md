---
title: "Processus de création d'UV"
layout: post
date: 2021-02-18 12:00                           # Laissez 2099 si c'est un brouillon. Pour l'heure,
                                                 # mettez au moins 2h avant l'heure actuelle (bug GitLab)
image: /assets/images/profile_pictures/all.png   # Apparaît lorsque vous partagez le lien
headerImage: false                               # Mettez 'true' pour mettre l'image précédente en entête du post
category: about                                   # Mettez 'projects' pour mettre le post dans Projets
author: eluscevu
tag:
  - UV
---

<p style="text-align: center;">
  <code style="color:#666;background:transparent;font-size:large">
    · Comment crée-t-on une UV ? ·
  </code>
</p>

On vous dit tout sur le processus de création d'une UV !

## Idée de départ

💡Tout part d'une idée : il faut qu'un.e professeur.e (ou un⋅e étudiant⋅e) ait un projet d'UV. Iel rédige le programme de l'UV, décide de son intitulé, rédige la description, définit les modalités d'évaluation, le volume horaire et le nombre de crédits attribués. Iel s'occupe également de contacter les intervenant.e.s envisagé.e.s pour assurer des cours, des TDs ou des TPs.

## Passage en Bureau de Département

👩‍🏫Iel contacte ensuite le.la directeur.rice du BDD (Bureau de Département, aussi appelé CoDep, Conseil de Département) correspondant ou le.la responsable du TC. Iel passe ensuite devant le bureau de département. Dans le cas d'une UV de tronc commun, il n'existe pas de bureau de département, le.la porteur.se de projet passe donc devant les responsables pédagogiques du TC et les délégué⋅e⋅s étudiant.e.s TC. Si le projet est jugé abouti, il est validé et voté. Si ce n'est pas le cas, le.la porteur.se de projet est invité.e à repasser lors du prochain BDD ou réunion, et à prendre en compte les recommandations du bureau.

## Passage au CÉVU

👩‍🏫Si le projet a été validé, le.la porteur.se contacte les VP du CÉVU (qui gèrent l'ordre du jour du CÉVU). Iel passe ensuite devant le CÉVU. Si le projet est jugé abouti par le CÉVU, il est voté et validé. Sinon iel devra repasser devant le CÉVU. Généralement le CÉVU propose des modifications et les transmet via le compte-rendu de la réunion.

## Mise en application

✅Le projet doit également être validé par la DFP (Direction de la Formation et de la Pédagogie) qui l'intègrera au catalogue des UVs et au planning. Le projet validé est ensuite mis en application le semestre décidé par le.la porteur.se de projet, généralement lors de la rentrée suivante.


![]({{site.url}}/assets/images/posts/creation-uv.png)
