---
title: "Rentrée Climat UTC 2020 - dispositif de sensibilisation"
layout: post
date: 2020-08-27 12:00                           # Laissez 2099 si c'est un brouillon. Pour l'heure,
                                                 # mettez au moins 2h avant l'heure actuelle (bug GitLab)
image: /assets/images/posts/RCU20_Affiche.png   # Apparaît lorsque vous partagez le lien
headerImage: true                               # Mettez 'true' pour mettre l'image précédente en entête du post
category: blog
author: eluscevu
tag:
  - Soutenabilité
---
### TheBigConf # Fresques du climat

Bonjour à tous !

Nous avons mis en place un dispositif de sensibilisation pour cette rentrée A2020 : c'est la [Rentrée Climat UTC](https://assos.utc.fr/integ/climat.php) !

Ouverts à tou·te·s (nouveau·elle·s, actuel·les ou ancien·ne·s étudiant·e·s ainsi qu'aux personnel·le·s de l'UTC), ces événements auront lieu pendant les deux premières semaines d'intégration, c'est-à-dire du 1er au 12 septembre.

Comme événements déjà fixés, il y a The Big Conf qui est une conférence pour apprendre comment on se sert de l'énergie actuellement dans le monde, notre dépendance à celle-ci, mais aussi comment fonctionne le système climatique ou encore le lien entre énergie et climat et les enjeux qui en découlent !

Avec les conditions particulières actuelles, on propose des conférences en présentiel mais qui seront aussi diffusées en direct sur Twitch ! Plus précisément, on ouvre 49 places en amphi par conférence pour les personnes qui ont des problèmes techniques (bugs de pc, manque de connexion) ou qui ont du mal à se concentrer en ligne :)


Mais il y a aussi : la Fresque du Climat qui est un atelier participatif permettant de mieux comprendre les causes et conséquences du dérèglement climatique, de prendre conscience de l'aspect complexe et systémique du système climatique, mais aussi d'échanger autour des thématiques environnementales avec d'autres personnes ! L'atelier de la Fresque du Climat a été spécialement adaptée au contexte UTCéen : il permet de découvrir et de discuter des solutions et initiatives environnementales sur Compiègne ! Cet atelier, lui, aura entièrement lieu en ligne.


Pour vous inscrire à un créneau de The Big Conf c'est ici : [https://www.weezevent.com/thebigconf-rcu20](https://www.weezevent.com/thebigconf-rcu20)

Pour vous inscrire à un créneau d'une Fresque du Climat c'est ici : [https://www.weezevent.com/rentree-climat-utc-2020](https://www.weezevent.com/rentree-climat-utc-2020)


N'hésitez pas à en parler autour de vous : c'est l'occasion de faire de la sensibilisation massive pour la rentrée ! On compte sur vous :-)

Bonne journée à tou·te·s !

_L'équipe de la Rentrée Climat UTC 2020_

____
[Lien vers la page Facebook RCU](https://www.facebook.com/fresque.utc.7)
