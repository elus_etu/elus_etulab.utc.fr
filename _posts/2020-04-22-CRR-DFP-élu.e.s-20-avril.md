---
title: "CR réunion DFP-élu.e.s 20 avril 2020"
layout: post
date: 2020-04-22 12:00                           # Laissez 2099 si c'est un brouillon. Pour l'heure,
                                                 # mettez au moins 2h avant l'heure actuelle (bug GitLab)
image: /assets/images/profile_pictures/all.png   # Apparaît lorsque vous partagez le lien
headerImage: true                               # Mettez 'true' pour mettre l'image précédente en entête du post
category: blog
author: eluscevu
tag:
  - CÉVU
---
# Compte-rendu de la réunion du 20 avril
## Questions des élu.e.s étudiants suite au sondage n°2 (représentés par Gwénaëlle, Mathieu, Solène, Pierre et Mathilde) à Étienne Arnoult (directeur à la formation et à la pédagogie), Antoine Jouglet (responsable formation ingénieur) et Pierre Feissel (vice-président du CÉVU).

### Remarques générales
Nous transmettons ici un compte-rendu qui fait part des échanges que l’on a eu dans le cadre d’une réunion informelle et donc pas de décisions officielles. Toutes les réponses ne sont donc pas définitives (même si certaines le sont).
Il a été mis en avant que de manière générale les enseignants manquent de retours et que les étudiants ne doivent pas hésiter à communiquer avec eux. Il faut prévenir les enseignants quand on se trouve en difficulté, que ce soit pour les cours ou pour les stages.

Pour des UVs qui posent de grosses problématiques pour un nombre conséquent d’étudiants, le principe pour trouver une solution est qu’il faut d’abord se tourner directement vers le responsable d’UV, puis si ça n’aboutit pas vers les élus concernés, puis vers la DFP.


### Questions générales
- Q1 *Problème de communication : les étudiant.e.s mais aussi les enseignant.e.s ne sont pas au courant de ce qui se passe.*

RÉPONSE : Durant cette semaine (de vacances), le directeur a travaillé sur le PRA et en a informé la DFP.
Les enseignants ont été informé de cela.
Notre demande va être prise en compte et la DFP va communiquer plus régulièrement sur les travaux en cours pour gérer la crise.
Côté élus étu' nous essayons aussi de vous donner un maximum d’informations même si nous pensons que des informations officielles plus fréquentes restent primordiales.

- Q2 *Y aura-t-il une modification du règlement des études ? Si oui à quelle échéance la connaîtra-t-on ? Pour la compétence internationale : dérogation à annoncer ?*

RÉPONSE : Ce n’est peut-être pas nécessaire.
Il y aura des adaptations à faire mais déclencher toute la « machine administrative » pour s’adapter à la crise ne paraît pas le plus pertinent.
Les adaptations à venir ne prendront pas forcément la forme d’une modification du règlement des études.
Oui il y aura une dérogation pour la compétence internationale (pour les départs de ce semestre et ceux du semestre prochain).
Pour la compétence internationale, le jury de diplôme saura prendre les bonnes décisions.
Ce sera entériné lors d’une décision de CEVU (a priori le 4 juin). Il y a donc un accord sur le principe mais c’est à finaliser administrativement.
Exemple : annotation sur le dossier pour qu’au moment où un étudiant passe en jury de diplôme il n’y ait pas de soucis.

- Q3 *Demander aux resp d’UV d’envoyer les modalités d’examen par mail et pas seulement lors de la visio.*

RÉPONSE : Un outil informatique existe pour renseigner les modalités d’examens. Une fois que tous les profs auront renseigné cet espace administratif, la DSI enverra automatiquement les modalités aux étudiants.

### Continuité pédagogique

- Q4 *Certains cours durent plus longtemps que 2h. Besoin d’une pause entre 12h et 14h ! On peut également rappeler qu’on a besoin d’une pause au milieu du cours.*

RÉPONSE : Un mail a été envoyé aux responsables d’UV ce weekend pour le respect des créneaux horaires.
L’info va être remise dans la gazette. Il a été constaté que malgré ces rappels cela n’était pas toujours totalement respecté.

- Q5 *Importance de l’organisation des cours et TDs en différé (enregistrements etc) pour les étudiants dans de moins bonnes conditions de travail → à généraliser --> enregistrer le direct avec l’appui de la CAP ou des étudiants*

RÉPONSE : La première solution est de s’organiser entre étudiants pour transmettre les notes de cours si le live n’est pas possible.
Faire du cas par cas en voyant chaque resp d’UV pour lui dire quand les problèmes se posent.
Inviter les étudiants à se signaler auprès des professeurs en cas de difficulté. Inviter les étu à diffuser leurs notes à toute l’UV quand c’est possible

- Q6 *Il faut réduire les programmes pour se concentrer sur un bon apprentissage des notions essentielles. Pourriez-vous faire une annonce officielle en ce sens ?*

RÉPONSE : D’un côté un certain nombre de collègues sont conscients de cette réalité et une communication en ce sens pourrait être faite.
D’un autre côté sur certaines UVs c’est assez difficile. On ne sait pas non plus de combien de semaines on dispose sur la fin du semestre. Crainte de pénaliser la poursuite d’études avec des prérequis qui ne seraient pas maîtrisés (grosses UV début de TC ou de branche).
D’ici à 3 semaines sous réserves de changement en fonction du PRA il sera nécessaire que les modalités d’évaluation et donc les contenus du programme soient fixés.

--> **Pour les élus étudiants il reste essentiel d'accepter que ce semestre n'est pas un semestre normal et que les programmes tout comme les exigences devront être revus en fonction.**


- Q7 *Est ce que c’est possible de choisir soi-même d’annuler son semestre si un étudiant.e n’était pas en mesure de travailler correctement, qu’à distance c’était trop compliqué, qu’il avait des problèmes de santé ou des problèmes familiaux ?*

RÉPONSE : Non mais c’est une décision de jury. Faire une demande motivée est possible mais cela reste une décision de jury. D'ailleurs, **on peut demander une interruption d’étude motivée.**
Pour cela il faut demander un formulaire au SAE (scola@utc.fr) puis le renvoyé rempli. D’habitude cela se fait pour des étudiants en difficulté.

- Q8 *Comment ça se passe pour les UV qui ne sont pas assurées ? Existe-t-il des modalités pour récupérer les crédits des cours non assurés ? La question se pose aussi pour les étudiant.e.s à l’étranger.*

RÉPONSE : pour l'étranger, il est malheureux que la ministre ait dit que chaque établissement doit s’assurer que les étudiants aient des semestres normaux car les moyens pour répondre à cela ne sont pas là. Il est ainsi difficile de pouvoir réagir aux étudiants qui disent “vous devez” nous assurer des UV.
On pourrait imaginer des solutions grâce aux API pour valider des crédits.
Pour les TX / PR : Les responsables d’UV vont être contactés pour voir comment transformer les TX de laboratoires pour ne pas perdre les crédits prévus (travail de mémoire bibliographique par exemple).

### Évaluation des UVs

- Q9 *Quelle réponse à notre vision des choses ?*

*Voici un rappel de notre vision des choses que nous avons clarifiée :*
**Réflexions sur l’évaluation / validation**

#### Notre vision :
Dans un premier temps, nous sommes contre des examens en présentiel ou à distance. Nous entendons par «examen» un sujet fermé à réaliser en 2h, à rendre sur moodle ou à faire sur table.
Nous proposons également de décorréler les modalités d’évaluation (avoir une note) de l’UV et celles de validation (avoir les crédits). L’évaluation serait là pour les étudiant.e.s, pour donner un retour sur leur travail et leur donner la possibilité de mesurer leur niveau de maîtrise des savoirs abordés dans l’UV, sans que cela ne présume de leur validation ou non de l’UV.
Nous proposons les modalités d’EVALUATION suivantes : des petits projets, des problèmes/exercices complémentaires à faire éventuellement en groupe à rendre en quelques jours, un porte-folio des rendus de TD etc.
Au sujet de la VALIDATION :
Pour la validation nous sommes pour un système ADMIS/UV ANNULE sans utiliser les lettres (A à F) ce semestre.
Validation : “sous condition d’assiduité”. Le fait de rendre des projets / des problèmes etc. est dans la situation actuelle déjà faire preuve de nombreuses compétences.
Donc on évalue l’effort des étudiant.e.s et le temps passé au travail. Les compétences validées ce semestre ne sont pas nécessairement celles du référentiel de l’UV, mais des compétences d’adaptation et de travail en conditions dégradées.
Pour les travaux trop faibles/non rendus, demander aux enseignant.e.s de contacter ces étudiant.e.s pour étudier à l’oral la situation et faire un rattrapage. Les personnes qui n’auront pas su répondre à des problèmes ouverts seront les personnes qui n’auront pas pu apprendre dans de bonnes conditions. Il sera important alors de comprendre si les étudiant.e.s en question ont fourni du travail tout au long du semestre ou s’ils ont décroché de l’UV.
Dans les conditions actuelles nous devons compter sur les étudiant.e.s qui savent s’auto-évaluer.
=> Les profs peuvent demander des autoévaluation à leurs étudiants et des étudiant.e.s peuvent dire eux-mêmes qu’ils ne sont pas au niveau et n’ont pas su acquérir les connaissances.
--------------FIN DE NOTRE VISION-----------

#### Complément sur notre vision :
Qu’est-ce que l’assiduité ? Travail rendu, examen réalisé au cours du semestre, présence aux visios, confirmation d’écoute des différents cours, résumés hebdomadaire des différents cours, etc.
Attention on parle ici de conditions de validation et non pas de conditions d’évaluation : on souhaite avoir des retours sur notre travail, on souhaite que les profs nous corrigent nos devoirs etc.
On valide si on a travaillé : si on montre qu’on fournit du travail on valide l’UV.
Si l’étudiant.e est conscient.e de ne rien avoir compris, de ne pas avoir fourni de travail etc : il y a une part importante d’auto-évaluation = si on est conscient qu’on a pas les compétences on demande à ne pas valider.
Argument de la règle de trois : les connaissances acquises dans le cadre universitaire ne présument pas vraiment de la réussite des UTCéens en entreprise ni du prestige du diplôme.


RÉPONSE : OK sur le fait de ne pas pouvoir évaluer les étudiants sur une échelle de A à F. MAIS le rôle des étudiants n’est pas de savoir comment évaluer les apprentissages. On ne va pas imposer des choses sur l’évaluation alors que les enseignants vont déjà faire preuve d’imagination. Les élus ne devraient pas chercher à imposer leur vision et laisser les professeurs trouver ces nouvelles modalités.
Sur les conditions d’attributions des UVs les jurys vont devoir s’adapter. Non : il y a des matières qui ne pourront pas être évaluées autrement qu’avec des devoirs fermés en temps limité. Les équipes pédagogiques ne pourront pas s’adapter comme cela.
VALIDATION : les équipes pédagogiques vont être conscientes que les modalités d’attribution des UVs, des crédits, vont être différentes. Les attributions d’UVs seront faites bien sûr avec une grande adaptation au contexte.
Notre crainte d’une évaluation qui ne soit pas à la hauteur des circonstances car inadapté au contexte est comprise. Cependant il faudrait faire confiance aux équipes pédagogiques. De plus, on ne pourra pas tout imposer et il y aura toujours des UVs où les profs ne vont pas bouger.
Les enseignants seront capables au niveau de la correction d’adapter leur barèmes. Il faut faire confiance aux collègues là dessus.


*Examens en présentiel* : cela semble difficilement faisable (étudiants éloignés de Compiègne, consignes de distanciation). L’idée de garder cela dans le PRA était de pouvoir s’adapter pour les cas particuliers, et ainsi de ne fermer la porte à rien. *→ Nous restons contre cette option côté élus étudiants et défendrons l’annulation de tout examen en présentiel lors de la réunion avec des membres du CÉVU jeudi.*
L’équipe enseignante ne serait pas non plus d’accord.
Étant donné la déclaration du président (pas de cours en présentiel) : les finaux n’auront probablement pas lieu comme d’habitude. --> La décision sera prise avec le Plan de Relance d’Activité qui devrait être publié en début de semaine prochaine.
**Calendrier de travail prévu** : cette semaine on discute du plan, le temps que les enseignants prennent les décisions, ça arriverait pas avant 3 semaines. Sous réserves de changements liés à l’évolution de la crise et aux discussions qui vont avoir lieu cette semaine, le PRA sera publié en début de semaine prochaine et les modalités d’examens et les programmes seront donc transmises autour de la semaine du 11 au 15 mai.
→**Ainsi il y a de fortes chances pour qu’il n’y ait plus de retour à Compiègne pour tous les étudiants mais la décision officielle ne tombera pas avant la semaine prochaine.**

Il semble impossible de réorienter des étudiants vu la situation. Conséquences pour la surcharge dans les UVs au semestre prochain : il y en aura mais on pourra l’absorber.
Le soucis de surcharge va surtout se poser pour tous les étudiants qui ne vont pas partir à l’étranger . 200 étudiants risquent de rester --> 1200 places à trouver...
Les étudiants qui vont souhaiter reprendre une UV il va y en avoir et cela va participer au problème. Toutes les UVs à TP ont été annulés or avant on refaisait une UV à TP sans refaire les TP mais là on ne pourra pas ne pas les refaire.

*À propos du calendrier du semestre :*
Une semaine de retard à la fin c’est faisable (durant la semaine prévue initialement des finaux ou la semaine avant les jurys) mais pas au delà. La fin du semestre printemps est en effet déjà l’organisation du semestre d’automne. Le semestre doit se terminer, jury compris, à la mi-juillet comme prévu.


___

- Q10 *Demander à prévoir le temps pour déposer les fichers sur moodle/scanner etc.*

RÉPONSE : On conseille aux étudiant.e.s de demander aux profs de prévoir un temps pour rendre les devoirs.

- Q11 *Pour l’instant qu’est ce qui est prévu pour les jurys ?*

RÉPONSE : « Tout étudiant qui veut être là le semestre prochain sera là au semestre prochain. » Les jurys s’adapteront à la situation. Pas de réorientation ce semestre.

### Étudiants en échange
 #### Étudiants dont le semestre a été annulé

- Q12 *Quelles solutions ont été étudiées pour pallier l’annulation de leur semestre ? Peuvent-ils valider des crédits par d’autres activités (suivi de MOOCs pré-sélectionés, projet extra-scolaire…) ?*

RÉPONSE : La validation de crédits par le suivi de MOOC semble trop compliqué à mettre en œuvre car cela demande beaucoup de temps pour vérifier la pertinence du MOOC et si il faut faire cela au cas par cas ce ne sera pas gérable. La DFP n’a pas le temps de faire une expertise du marché des MOOCs. C’est très difficile à gérer quand les universités partenaires annoncent une annulation après le début de notre semestre. Avant ça va encore (on s’est adapté pour l’UTSEUS) mais sinon cela devient impossible. Quand on part à l’étranger les cours sont reconnus mais c’est une convention de 2 ans de tractations pour se mettre d’accord, pour les MOOCs il faudrait le même travail.

#### Étudiants en études à l’étranger à distance

- Q13 *La continuité n’est pas assurée dans toutes les UVs et tous les crédits prévus ne seront pas validés : quid du passage en branche ?*
        ◦ Ex: TC04 qui n’ont pas assez de crédits

RÉPONSE : Devront faire un semestre supplémentaire si ils n’ont pas les crédits. Bien sûr ça les pénalise mais c’est comme tous les TC05 c’est un semestre mi-TC mi-Branche. Et cela peut se rattraper (il y a souvent le cas) dans les semestre de branches restant.

- Q14 *“Je suis les cours depuis la France, mais si les examens sont en présentiel en Argentine je ne pourrai pas les faire : que dois-je faire ?”*

RÉPONSE :  On a déjà mis en place des choses par le passé : si l’université partenaire est d’accord : on convient d’un jour et d’une heure pour les examens et on organise l’examen de notre côté.

- Q15 *Peut-on repartir dans la destination prévue au prochain semestre (si on suit les cours à distance de la destination prévue)*

RÉPONSE : Non
Bcp d’universités partenaires annulent au fil des jours, la DRI prévient au fur et à mesure qu’elle reçoit les informations, il n’y a pas de date limite pour annoncer l’annulation. Mais du coup si l’échange est annulé avant le début de la rentrée prochaine on pourra les intégrer au semestre d’automne (comme UTSEUS).
Pour le moment cela reste minoritaire mais le nombre d’annulation va surement augmenter fortement. Prendre contact avec Mme Marin pour avoir plus de détails.

- Q16 *Serait-il possible d’avoir un examen de la validation de la compétence internationale par critère (donc sans départ) avant le jury de diplôme afin de ne pas avoir de mauvaise surprise post-TN10 ? Des étudiants aimeraient être sûr que leur compétence internationale est validée avant de partir en TN10 et de ne pas le découvrir dans un refus par le jury de diplôme. (question posée avant la crise du covid19)*

RÉPONSE : La DSI a développé une application qui permet de faire ça sur l’ENT (valider chaque critère) et ensuite le responsable de branche entérine que l’étudiant a validé sa compétence internationale. L’application est fonctionnelle et a été validée en conseil pédagogique. Elle devait être mise en production il y a quelques semaines. Donc c’est retardé mais ça va bientôt sortir.

### Stages
#### Général

- Q17 *Incertitudes pour l’évaluation des stages et la validation des 30 crédits : flou total sur les rendus, pas assez de communication claire*

RÉPONSE : il y aura bien un rapport et une soutenance. Pour un stage de 18 semaines, le rapport sera constitué aux 3/4 de ce qui a été fait en entreprise et 1/4 sur le travail complémentaire.
C’est aux responsables pédagogiques et tuteurs de stages de préciser au cas par cas les modalités du travail complémentaire à effectuer.

- Q18 *Sujet de stage remis en cause par la crise : comment cela sera pris en compte lors de l’évaluation ?*

RÉPONSE : Dans ce cas le travail du stage évolue certes, et il est différent mais c’est très rare que le sujet n’évolue pas, en effet il évolue très souvent. Que les travaux demandés changent ce n’est pas un problème. Le rapport doit être fourni en conséquence, même si le sujet change il faut faire un rapport sur le travail fait. Et si les travaux demandés sont trop “faibles”, cela va dépendre des branches/filière, il est important d’en informer le suiveur pour ne pas que l’UTC “découvre” ce genre de situation fin août. Alerter tout de suite si y a un problème.
Il faut inviter les étudiants à s’exprimer directement vers leurs resps pédago, leurs suiveurs, ou sinon les élus (pas envoyer ses parents !)

- Q19 *L’UTC peut-elle faire pression pour aller contre la décision de certaines entreprises de suspendre les stages alors que le télétravail serait possible ?*

RÉPONSE : Très difficile, UTC pourrait discuter et l’a déjà fait, mais les décisions d’entreprises paraissent surprenantes et peuvent pour autant être justifiées en interne. Il y a zéro moyen de pression. Les entreprises font les choses dans la légalité. Mais c’est très dur pour les étudiants notamment concernant les indemnités de stage.
Cependant, il est déjà arrivé que des arrangements soient trouvés et cela est toujours possible.

- Q20 *Quelles consignes/conseils pour la recherche de stage pour A20 ? Décalage des dates, suivi … ?*

RÉPONSE : Les dates ont déjà été repoussées, pour la recherche il y a le forum virtuel Booste ton CV le 14 mai, et un autre forum à distance pour chercher les stages (début juin), et habituellement à cette dates les conventions sont déjà bouclées, donc oui il y aura de la souplesse.

#### TN10

- Q21 *Quelles sont les conditions du travail supplémentaire dans le cas d’un stage réduit à 18 semaines ? Pour le TN10 est-il possible de faire un semestre supplémentaire à base de projets/TX/AP ou un autre stage pour les 2 mois manquants ?*

RÉPONSE : pour le TN10 le mieux est de refaire un stage. TN10 est une fois sur deux un premier job donc il vaut mieux repartir sur un stage sain qui vous donne plus de chance de trouver le 1er job que d’essayer de faire des petits morceaux difficiles à vendre sur un CV. Sur un TN10 rien ne remplace le contact direct entreprise/stagiaires qui se concrétise souvent sur une offre d’emploi. Donc conseil de viser plutôt un autre stage mais le semestre supplémentaire sera possible.

- Q22 *Les soutenances de stage vont elles être décalées pour ceux dont le stage est suspendu ?*

RÉPONSE : Pas encore tranché. Ligne défendue : la soutenance de stage se fait traditionnellement à la fin du stage. Mais le rôle de la soutenance c’est de montrer qu’on est capable de montrer à un public qu’on est connaisseur, qu’on a compris etc. Pour ça il y a pas forcément besoin d’attendre la fin du stage. On pourrait donc le faire alors que le stage n’est pas fini.
Le rapport lui viendra à la fin (présentant toute la démarche et tous les résultats)
Question des jurys de diplôme qui sont en octobre : une solution a été trouvée, les présidents des jurys de diplôme verront tous les candidats y compris ceux en cours d’achèvement du TN10 et pourront délivrer le diplôme même si le stage n’est pas fini.

- Q23 *Quid des session du TOEIC ?*

RÉPONSE : Niveau TOEIC on est bloqué par ETS. Si les étudiants trouvent d’autres solutions (TOEFL, GRE ?, …) Ils peuvent y aller car pour le moment le TOEIC est bloqué.
Pour être diplômée en novembre : est-il possible d’envisager une session cet été ou en septembre ?
Ce sera au cas par cas .



________________________________________________________

Une réunion avec des membres du CÉVU est prévue ce jeudi 23 avril à 14h. Au cours de cette réunion sera examiné le projet de plan de reprise d'activité (PRA) de l'UTC. À l'issue de cette réunion nous en saurons donc plus et pourrons vous faire part des réflexions. Nous sommes actuellement en train d'échanger avec les membres du CÉVU sur notre équipe Mattermost dédiée.

________________________________________________________

# Rappel
**Quelques contacts utiles :**

    - Notre service de médecine préventive: medecine-preventive@utc.fr
    - Notre psychologue : Catherine Carpentier : carpentier@utc.fr
    - Des aides psychologiques extérieures à l’UTC:
    - https://psysolidaires.org/: psychologues
    - http://www.terrapsy.org/ accompagnement psychologique en français, anglais et arabe
    - Cellule d’aide psychologique nationale : 0 800 130 000
    - https://nightline-paris.fr/: Chat en français et anglais tenu par des étudiants
