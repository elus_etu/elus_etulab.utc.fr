---
title: "Rappel aides sociales"
layout: post
date: 2020-06-02 22:00                           # Laissez 2099 si c'est un brouillon. Pour l'heure,
                                                 # mettez au moins 2h avant l'heure actuelle (bug GitLab)
image: /assets/images/profile_pictures/all.png   # Apparaît lorsque vous partagez le lien
headerImage: false                               # Mettez 'true' pour mettre l'image précédente en entête du post
category: blog
author: eluscevu
tag:
  - social
---
# Aides pour les étudiant⋅es en difficulté financière

**Si vous êtes dans une situation financière difficile, voici un rappel des aides disponibles :**

- En premier lieu : *il faut prendre RDV avec l'assistant social du CROUS*
--> par mail : action-sociale@crous-amiens.fr ou  directement en ligne sur https://www.messervices.etudiant.gouv.fr [rubrique "Prendre RDV avec le CROUS"]. C'est un⋅e professionnel⋅le qui saura évaluer ta situation et te rediriger au mieux vers les aides adaptées. Souvent, et dans un premier temps, est donné un bon d'achat alimentaire de 50 €. Ensuite, le CROUS peut mettre en place des aides d'urgence lors d'une commission qui se réunit régulièrement. Ce sont ces aides qui ont vu leur enveloppe augmenter de 10 millions d'euros au niveau national pour faire face aux effets de la crise sanitaire. Elles peuvent être ponctuelles ou durer quelques mois.

- Une aide exeptionnelle de 200€ a été mise en place très récemment par le ministère pour venir en aide aux étudiant⋅es ultra-marins ou ceux faisant face à la suspension de leur emploi ou de leur stage gratifié (sans justificatifs de ressources). Plus d'info ici : https://actualites.utc.fr/precisions-sur-laide-exceptionnelle-aux-etudiants-annoncee-le-4-mai-2020-par-le-gouvernement/

- En cas de non prise en charge par le CROUS, ou d'une situation urgente, une aide exceptionnelle est mise en place grâce au FSDIE social (Fonds de solidarité et de développement des initiatives étudiantes) et à la CVEC (Contribution de Vie Etudiante et de Campus): vous devez télécharger le dossier (https://cloud.utc.fr/index.php/s/28o34NDF7LxC2ma). Des pièces justificatives vous seront demandées (notamment tous les relevés bancaires) et le dossier sera étudié en commission dans les meilleurs délais. Une aide financière pourra alors vous être proposée (délai d'une à deux semaines après le dépot du dossier).
Ces aides sont souvent de quelques centaines d'euros et permettent aux étudiants de faire face dans l'immédiat à leur situation d'urgence avant de trouver une solution avec le CROUS ou avant un retour à la normale de la situation. Avec la crise sanitaire, cette aide peut s'adresser à des étudiant⋅es qui font face à une rupture exceptionnelle de leurs revenus (suspension de stage, de job étudiant, parents en difficulté financière...) mais aussi permettre à des étudiants qui sont dans une situation difficile d'augmenter leur forfait internet (aide de 50 €) ou de procéder à des achats de matériel informatique (jusqu'à 500 €) pour pouvoir s'adapter à l'enseignement à distance, en plus d'éventuelles aides du CROUS.

En cas de soucis pour le dépôt du dossier FSDIE, vous pouvez contacter Véronique Hédou (hedou@utc.fr), Pierre Kidzie (pierre.kidzie@etu.utc.fr) ou Solène Brasseur (solene.brasseur@etu.utc.fr).

- Autre rappel : Les bénéficiaires d’EPI peuvent se tourner vers La Passerelle (13 Quai du Clos des Roses, 60200 Compiègne - 03 44 86 32 35). Pour les non bénéficiaires qui en auraient besoin, merci de contacter EPI : epi@assos.utc.fr

...

_Dans tous les cas, n'hésitez pas à faire remonter à votre conseiller, votre responable pédagogique ou vos élus (CÉVU ou de votre cursus) toute difficulté recontrée pour obtenir un soutien et un suivi le meilleur possible._
