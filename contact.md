---
title: Contact
layout: page
---
![Profile Image]({% if site.external-image %}{{ site.picture }}{% else %}{{ site.url }}/{{ site.picture }}{% endif %})

# Les élu⋅es étudiant⋅es de l'UTC

## Qui sommes nous ?

Ce site a été mis à jour par **l'équipe d'élu⋅es étudiant⋅es du mandat A19-A21**, élue lors des éléctions universitaires de novembre 2019 et des élections partielles de novembre 2020. Il continue d'être alimenté par le mandat des élu⋅es A21-A23.

Représentant l'ensemble des étudiant⋅es, nous siégons dans les différentes instances de l'UTC, comme le Conseil des Études et de la Vie Universitaire (CÉVU), le Conseil d'Administration (CA), ou les conseils de départements (voir la [boîte à outils]({{site.url}}/about) pour plus d'informations sur les instances).
Les sujets que nous abordons vont de l'organisation de la vie étudiante à la mise en place d'une fillière au sein d'une branche, en passant par la stratégie ou les budgets de l'établissement.

<!--Tu peux retrouver des explications sur le fonctionnement et le rôle des instances de l’UTC en suivant ces deux liens vers les sites internet des listes de la dernière campagne : [https://utco.fr/conseils](https://utco.fr/conseils) ; [https://utransformation.fr/comprendre-lutc/](https://utransformation.fr/comprendre-lutc/)-->
L'équipe du présent mandat est constituée en grande partie par la liste [UTransFORMATION](https://utransformation.fr) (site à jour pour l'élection de 2019 uniquement), unique candidate dans plusieurs instances. Elle est axée vers la tranformation écologique et sociale de l'établissement, en participant à tous les projets de soutenabilité en relation avec l'administration de l'UTC et de ses départements.

Tu peux une [vidéo de présentation des trois principales instances](https://webtv.utc.fr/watch_video.php?v=WHN68X3G117D) à savoir le Conseil d'Administration, le Conseil Scientifique, et le Conseil des Études et de la Vie Universitaire.

Toutes les informations officielles sont à retrouver sur l'ENT, dans le menu, rubrique _Politique d'établissement_.  Tu y trouveras par exemple la [liste des membres des conseils centraux](https://webapplis.utc.fr/ent/services/services.jsf?sid=262).

## Comment nous contacter ?

Ci-dessous trois différentes possibilités pour nous joindre, à privilégier dans l'ordre suivant :

### 1. Équipe publique Mattermost des élu⋅es étu' sur team.picasoft.net

Mattermost est un logiciel libre de _chat_, semblable à Slack, dont [l'association Picasoft](https://picasoft.net/)[^note] héberge une instance sur ses serveurs.
Un espace de discussion public (une "équipe") y a été créé spécifiquement par les élu⋅es étu', sur lequel un canal est dédié à chaque conseil ou thématique.

L'objectif de cette équipe est de favoriser les échanges et les questions, en permettant le libre accès à toutes les réponses données.
Il est aussi fortement bienvenu de suggérer des idées, de débattre ou même s'impliquer dans un groupe de travail à nos côtés(sur l'ingénierie soutenable, ou l'amélioration de la vie étudiante par exemple) !

→ Si tu as déjà un compte, [retrouve l'équipe ici](https://team.picasoft.net/elues-etu-utc).

→ Si tu n'as pas de compte, [c'est par là](https://team.picasoft.net/signup_user_complete/?id=mbhk9msk878h9dk4zmpxefmako).

### 2. Nous envoyer un mail

Les élu⋅es étudiant⋅es de chaque instance disposent d'un mail, dont tu trouveras le détail ci-dessous. Nous tenterons de te répondre dans les meilleurs délais.

▪️ **CEVU** : _eluscevu@utc.fr_ : C'est à eux que tu peux poser des questions concernant les enseignements dans leur globalité, et c’est notamment eux que tu peux contacter si tu as des propositions d’amélioration de la vie universitaire, ou encore de création d’UVs.

▪️ **CA** : _elusca@utc.fr_ : C'est à eux que tu peux poser des questions concernant la politique, la stratégie, le budget et les partenaires de l'établissement.

▪️ Les élu⋅es des **Conseils de Département** : ce sont eux qui gèrent les enseignements et la vie du département en général.

→ GB : _elusgb@utc.fr_           → GI : _elusgi@utc.fr_              → IM : _elusim@utc.fr_             → GP : _elusgp@utc.fr_
→ GU : _elusgsu@utc.fr_          → TSH : _elustsh@utc.fr_         

▪️ **Délégués TC** : _elustc@utc.fr_ : C'est à eux que tu peux faire remonter tes remarques concernant les enseignements de TC et de Hutech.

▪️ **CS** : _eluscs@utc.fr_ . Ce conseil concerne la recherche et donc plus particulièrement les doctorants. Tu peux ainsi leur poser des questions ou leur faire des remarques concernant les doctorats. Malheureusement il n’y a pas eu de candidats aux dernières élections... (contacter les autres élu⋅es)

▪️ **Master** : _eluscpm@utc.fr_ . Ce sont eux qui traitent les questions liées aux masters.


### 3. Venir nous voir à la MDE

Nous disposons d'un local dans la MDE, en FE205 (deuxième étage au dessus du Pic'Asso), dans lequel nous tiendrons quelques permanences par semaine sur le temps du midi. Tu pourras venir nous y rencontrer, poser tes questions, si possible après nous avoir envoyé un mail à _eluscevu@utc.fr_. Les horaires des permanences seront indiquées ici prochainement.


Nous disponsons aussi d'une [page facebook](https://www.facebook.com/elusUTC/).



## Autre contact à retenir

Pour répondre aux situations d’urgence financière, de nombreuses aides sociales pour les étudiants existent. Elles sont néanmoins encore trop peu connues et utilisées. Pour remédier à cette situation, le ministère de l’Enseignement supérieur, de la Recherche et de l’Innovation met en place à partir du 10 janvier 2020 à 9h un numéro d’appel, le **0 806 000 278**.



------------



[^note]: _Picasoft est une association de l'Université de Technologie de Compiègne fondée en 2016 par des étudiants et des enseignants. Elle a pour objet de promouvoir et défendre une approche libriste, inclusive, respectueuse de la vie privée, respectueuse de la liberté d'expression, respectueuse de la solidarité entre les humains et respectueuse de l'environnement, notamment dans le domaine de l'informatique._
